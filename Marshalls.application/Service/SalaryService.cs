﻿
using Marshalls.Domain.Entities;
using System.Collections.Generic;
using DomainCotract = Marshalls.Domain.Contracts;

namespace Marshalls.Application.services
{
    public class SalaryService : ISalaryService
    {
        readonly DomainCotract.ISalaryRepository _domainICotract;
        public SalaryService(DomainCotract.ISalaryRepository domainICotract)
        {
            this._domainICotract = domainICotract;
        }

        public List<SalaryDTO> GetSalaries()
        {
            return _domainICotract.GetSalaries();
        }

        public List<SalaryDTO> GetSalarysFilter(string EmployeeCode, int filter)
        {
            return _domainICotract.GetSalarysFilter(EmployeeCode, filter);
        }

        public List<PositionDTO> GetPositios()
        {
            return _domainICotract.GetPositios();
        }

        public List<DivisionDTO> GetDivisios()
        {
            return _domainICotract.GetDivisios();
        }

        public List<OfficeDTO> GetOffices()
        {
            return _domainICotract.GetOffices();
        }

        public EmployeeDTO GetEmployee(string EmployeeCode)
        {
            return _domainICotract.GetEmployee(EmployeeCode);
        }

        public List<SalaryDetailDTO> GetSalaryDetail(string EmployeeCode)
        {
            return _domainICotract.GetSalaryDetail(EmployeeCode);
        }

        public string saveSalarys(List<Salary> ListSalary)
        {
            return _domainICotract.saveSalarys(ListSalary);
        }

        public EmployeeBonoDTO GetEmployeeBono(string EmployeeCode)
        {
            return _domainICotract.GetEmployeeBono(EmployeeCode);
        }

        public List<SalaryBonoDTO> GetSalaryBono(string EmployeeCode)
        {
            return _domainICotract.GetSalaryBono(EmployeeCode);
        }

    }
}
