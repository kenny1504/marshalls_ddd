﻿using Marshalls.Domain.Entities;
using System.Collections.Generic;

namespace Marshalls.Application.services
{
    public interface ISalaryService {

        List<SalaryDTO> GetSalaries();
        List<SalaryDTO> GetSalarysFilter(string EmployeeCode, int filter);
        List<PositionDTO> GetPositios();
        List<DivisionDTO> GetDivisios();
        List<OfficeDTO> GetOffices();
        EmployeeDTO GetEmployee(string EmployeeCode);
        List<SalaryDetailDTO> GetSalaryDetail(string EmployeeCode);
        string saveSalarys(List<Salary> ListSalary);
        EmployeeBonoDTO GetEmployeeBono(string EmployeeCode);
        List<SalaryBonoDTO> GetSalaryBono(string EmployeeCode);
    }
}
