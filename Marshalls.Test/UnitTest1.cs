using api.Controllers;
using Marshalls.Application.services;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using Xunit;

namespace Marshalls.Test
{
    public class UnitTest
    {

        [Fact]
        public void Test1()
        {
            var ISalaryServiceStub = new Mock<ISalaryService>();
            var loggerStub = new Mock<ILogger<SalaryController>>();

            var controller = new SalaryController(ISalaryServiceStub.Object, loggerStub.Object);

            var EmployeeCode = "00000009";
            var filter = 1;
            var response = controller.GetSalarysFilter(EmployeeCode, filter);

            Assert.Null(response);
        }
    }
}
