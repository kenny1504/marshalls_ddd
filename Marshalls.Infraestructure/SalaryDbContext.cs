﻿using Marshalls.Domain.Entities;
using Microsoft.EntityFrameworkCore;


namespace Marshalls.Infraestructure
{
    public class SalaryDbContext : DbContext
    {
        public SalaryDbContext(DbContextOptions<SalaryDbContext> options) : base(options) { }

        public DbSet<Salary> Salarys { get; set; }

        public DbSet<Office> Offices { get; set; }

        public DbSet<Position> Positions { get; set; }

        public DbSet<Division> Divisions { get; set; }

    }
}
