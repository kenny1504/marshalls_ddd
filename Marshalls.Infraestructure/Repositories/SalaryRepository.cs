﻿using Marshalls.Domain.Contracts;
using Marshalls.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;


namespace Marshalls.Infraestructure.Repositories
{
    public class SalaryRepository : ISalaryRepository
    {
        private SalaryDbContext _context;
        public SalaryRepository(SalaryDbContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public List<SalaryDTO> GetSalaries()
        {
            var query =
            from salary in _context.Salarys
            join division in _context.Divisions on salary.DivisionId equals division.Id
            join position in _context.Positions on salary.PositionId equals position.Id
            select new
            {
                salary,
                division,
                position
            };


            var x = query.AsEnumerable().GroupBy(x => x.salary.EmployeeCode).AsEnumerable().Select(
                a =>
                     new SalaryDTO
                     {
                         EmployeeCode = a.Key,
                         EmployeeCompleteName = a.FirstOrDefault().salary.EmployeeName + " " + a.FirstOrDefault().salary.EmployeeSurname,
                         Division = a.OrderByDescending(x => x.salary.Year).ThenByDescending(x => x.salary.Month).Select(x => x.division.Name).FirstOrDefault(),
                         Position = a.OrderByDescending(x => x.salary.Year).ThenByDescending(x => x.salary.Month).Select(x => x.position.Name).FirstOrDefault(),
                         Grade = a.OrderByDescending(x => x.salary.Year).ThenByDescending(x => x.salary.Month).Select(x => x.salary.Grade).FirstOrDefault(),
                         BeginDate = a.OrderByDescending(x => x.salary.Year).ThenByDescending(x => x.salary.Month).Select(x => x.salary.BeginDate).FirstOrDefault(),
                         Birthday = a.FirstOrDefault().salary.Birthday,
                         IdentificationNumber = a.FirstOrDefault().salary.IdentificationNumber,

                         TotalSalary = a.OrderByDescending(x => x.salary.Year)
                          .ThenByDescending(x => x.salary.Month)
                          .Select(x => x.salary.BaseSalary + x.salary.ProductionBonus + (x.salary.CompensationBonus * 0.75M) + (((x.salary.BaseSalary + x.salary.Commission) * 0.08M) + x.salary.Commission) - x.salary.Contributions)
                          .DefaultIfEmpty(0).FirstOrDefault()

                     }
            );
            return x.ToList();
        }

        public List<SalaryDTO> GetSalarysFilter(string EmployeeCode, int filter)
        {

            var getEmployee = GetEmployee(EmployeeCode);
            if (getEmployee == null) return new List<SalaryDTO>();

            var query =
                from salary in _context.Salarys
                join division in _context.Divisions on salary.DivisionId equals division.Id
                join position in _context.Positions on salary.PositionId equals position.Id
                join office in _context.Offices on salary.OfficeId equals office.Id
                select new
                {
                    salary,
                    division,
                    position
                };


            var x = query.AsEnumerable().GroupBy(x => x.salary.EmployeeCode).AsEnumerable().Select(
                a =>
                     new
                     {
                         EmployeeCode = a.Key,
                         EmployeeCompleteName = a.FirstOrDefault().salary.EmployeeName + " " + a.FirstOrDefault().salary.EmployeeSurname,
                         Division = a.OrderByDescending(x => x.salary.Year).ThenByDescending(x => x.salary.Month).Select(x => new { x.salary.DivisionId, x.division.Name }).FirstOrDefault(),
                         Position = a.OrderByDescending(x => x.salary.Year).ThenByDescending(x => x.salary.Month).Select(x => new { x.salary.PositionId, x.position.Name }).FirstOrDefault(),
                         Office = a.OrderByDescending(x => x.salary.Year).ThenByDescending(x => x.salary.Month).Select(x => x.salary.OfficeId).FirstOrDefault(),
                         Grade = a.OrderByDescending(x => x.salary.Year).ThenByDescending(x => x.salary.Month).Select(x => x.salary.Grade).FirstOrDefault(),
                         BeginDate = a.OrderByDescending(x => x.salary.Year).ThenByDescending(x => x.salary.Month).Select(x => x.salary.BeginDate).FirstOrDefault(),
                         a.FirstOrDefault().salary.Birthday,
                         a.FirstOrDefault().salary.IdentificationNumber,
                         Month = a.OrderByDescending(x => x.salary.Year).ThenByDescending(x => x.salary.Month).Select(x => x.salary.Month).FirstOrDefault(),

                         TotalSalary = a.OrderByDescending(x => x.salary.Year)
                          .ThenByDescending(x => x.salary.Month)
                          .Select(x => x.salary.BaseSalary + x.salary.ProductionBonus + (x.salary.CompensationBonus * 0.75M) + (((x.salary.BaseSalary + x.salary.Commission) * 0.08M) + x.salary.Commission) - x.salary.Contributions)
                          .DefaultIfEmpty(0).FirstOrDefault()

                     }
            );

            /*** Filtro
             * 1 =  Filtra Empleados con la misma Oficina y Grado
             * 2 =  Filtra Empleados de todas las Oficinas y con el mismo Grado
             * 3 =  Filtra Empleados con la misma Posición y Grado
             * 4 =  Filtra Empleados con el mismo Grado y el mismo mes de Ingreso
             */

            return x.Where(q =>
                filter == 1
                    ? q.Office == getEmployee.OfficeId && q.Grade == getEmployee.Grade
                    : filter == 2
                        ? q.Grade == getEmployee.Grade
                        : filter == 3
                            ? q.Position.PositionId == getEmployee.PositionId && q.Grade == getEmployee.Grade
                            : q.Grade == getEmployee.Grade && q.Month == getEmployee.Month).Select(x => new SalaryDTO
                            {
                                EmployeeCode = x.EmployeeCode,
                                EmployeeCompleteName = x.EmployeeCompleteName,
                                Division = x.Division.Name,
                                Position = x.Position.Name,
                                Grade = x.Grade,
                                BeginDate = x.BeginDate,
                                Birthday = x.Birthday,
                                IdentificationNumber = x.IdentificationNumber,
                                TotalSalary = x.TotalSalary

                            }).ToList();
        }

        public EmployeeDTO GetEmployee(string EmployeeCode)
        {
            return _context.Salarys.Where(x => x.EmployeeCode == EmployeeCode)
                .OrderByDescending(x => x.Year)
                .ThenByDescending(x => x.Month)
                .Select(x => new EmployeeDTO
                {
                    EmployeeCode = x.EmployeeCode,
                    IdentificationNumber = x.IdentificationNumber,
                    EmployeeName = x.EmployeeName,
                    EmployeeSurname = x.EmployeeSurname,
                    Birthday = x.Birthday,
                    BeginDate = x.BeginDate,
                    DivisionId = x.DivisionId,
                    OfficeId = x.OfficeId,
                    PositionId = x.PositionId,
                    Grade = x.Grade,
                    Month = x.Month
                }).FirstOrDefault();
            ;
        }

        public List<PositionDTO> GetPositios()
        {
            return _context.Positions.Select(x => new PositionDTO { Id = x.Id, Name = x.Name }).ToList();
        }

        public List<DivisionDTO> GetDivisios()
        {
            return _context.Divisions.Select(x => new DivisionDTO { Id = x.Id, Name = x.Name }).ToList();
        }

        public List<OfficeDTO> GetOffices()
        {
            return _context.Offices.Select(x => new OfficeDTO { Id = x.Id, Name = x.Name }).ToList();
        }

        public List<SalaryDetailDTO> GetSalaryDetail(string EmployeeCode)
        {
            return _context.Salarys.Where(x => x.EmployeeCode == EmployeeCode)
                .Select(x => new SalaryDetailDTO
                {
                    Id = x.Id,
                    Year = x.Year,
                    Month = x.Month,
                    Salary = x.BaseSalary,
                    ProductionBonus = x.ProductionBonus,
                    CompensationBonus = x.CompensationBonus,
                    Commission = x.Commission,
                    Contributions = x.Contributions
                }).ToList();

        }

        public string saveSalarys(List<Salary> ListSalary)
        {
            try
            {
                var first = ListSalary.FirstOrDefault();

                if (_context.Salarys.Any(w => w.EmployeeName == first.EmployeeName
                   && w.EmployeeSurname == first.EmployeeSurname
                   && w.IdentificationNumber != first.IdentificationNumber)) return "No Es Posible Almacenar Este Empleado. Ya existe!";

                //Guarda y/o actualiza
                _context.Salarys.UpdateRange(ListSalary);
                _context.SaveChanges();

                //Datos eliminados
                var actuales = _context.Salarys.Where(e => e.EmployeeCode == first.EmployeeCode).ToList();
                var eliminados = actuales.Except(ListSalary).ToList();
                if (eliminados.Any())
                {
                    _context.Salarys.RemoveRange(eliminados);
                    _context.SaveChanges();
                }

                return "Acción realizada correctamente";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public List<SalaryBonoDTO> GetSalaryBono(string EmployeeCode)
        {
            return _context.Salarys.Where(x => x.EmployeeCode == EmployeeCode)
                .Select(x => new SalaryBonoDTO
                {
                    Year = x.Year,
                    Month = x.Month,
                    BeginDate = x.BeginDate,
                    Salary = x.BaseSalary
                }).ToList();
        }

        public EmployeeBonoDTO GetEmployeeBono(string EmployeeCode)
        {
            var query =
            (from salary in _context.Salarys
             join division in _context.Divisions on salary.DivisionId equals division.Id
             join position in _context.Positions on salary.PositionId equals position.Id
             where salary.EmployeeCode == EmployeeCode
             select new
             {
                 salary.EmployeeName,
                 salary.EmployeeSurname,
                 Position = position.Name,
                 Division = division.Name,
                 salary.Year,
                 salary.Month

             });

            return query.OrderByDescending(x => x.Year)
                .ThenByDescending(x => x.Month)
                .Select(x => new EmployeeBonoDTO
                {
                    EmployeeCompleteName = x.EmployeeName + " " + x.EmployeeSurname,
                    Division = x.Division,
                    Position = x.Position

                }).FirstOrDefault();
        }

    }

}
