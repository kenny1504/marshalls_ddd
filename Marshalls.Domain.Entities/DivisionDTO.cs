﻿
namespace Marshalls.Domain.Entities
{
    public class DivisionDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
