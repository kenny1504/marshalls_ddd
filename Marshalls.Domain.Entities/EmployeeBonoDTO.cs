﻿
namespace Marshalls.Domain.Entities
{
    public class EmployeeBonoDTO
    { 
        public string EmployeeCompleteName { get; set; }
        public string Division { get; set; }
        public string Position { get; set; }
    }
}
