﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Marshalls.Domain.Entities
{
    public class SalaryDTO
    {
       
        public string EmployeeCode { get; set; }

        public string EmployeeCompleteName { get; set; }

        public string Division { get; set; }

        public string Position { get; set; }

        public int Grade { get; set; }

        public DateTime BeginDate { get; set; }

        public string BeginDates => BeginDate.ToString("dd/MM/yyyy");

        public DateTime Birthday { get; set; }

        public string Birthdays => Birthday.ToString("dd/MM/yyyy");

        public string IdentificationNumber { get; set; }

        public decimal TotalSalary { get; set; }

    }
}
