﻿using System;

namespace Marshalls.Domain.Entities
{
    public class EmployeeDTO
    {
        public string EmployeeCode { get; set; }
        public string IdentificationNumber { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeSurname { get; set; }
        public DateTime Birthday { get; set; }
        public string Birthdays => Birthday.ToString("yyyy-MM-dd");
        public DateTime BeginDate { get; set; }
        public string BeginDates => BeginDate.ToString("yyyy-MM-dd");
        public int DivisionId { get; set; }
        public int PositionId { get; set; }
        public int OfficeId { get; set; }
        public int Grade { get; set; }

        public int Month { get; set; }
    }
}
