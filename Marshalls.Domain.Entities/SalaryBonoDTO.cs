﻿using System;

namespace Marshalls.Domain.Entities
{
    public class SalaryBonoDTO
    {
        public int Year { get; set; }
        public int Month { get; set; }
        public DateTime BeginDate { get; set; }
        public string BeginDates => BeginDate.ToString("dd/MM/yyyy");
        public decimal Salary { get; set; }

    }
}
