﻿
namespace Marshalls.Domain.Entities
{
    public class OfficeDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
