﻿
namespace Marshalls.Domain.Entities
{
    public class Position
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual Salary Salary { get; set; }
    }
}
