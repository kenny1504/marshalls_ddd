﻿using System.Collections.Generic;
using Marshalls.Application.services;
using Marshalls.Domain.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SalaryController : Controller
    {
        private readonly ISalaryService _iSalaryService;

        private readonly ILogger<SalaryController> _logger;

        public SalaryController(ISalaryService iSalaryService, ILogger<SalaryController> logger)
        {
            this._iSalaryService = iSalaryService;
            _logger = logger;
        }


        [HttpGet]
        public List<SalaryDTO> Get()
        {
            List<SalaryDTO> response = new List<SalaryDTO>();
            try
            {
                response = _iSalaryService.GetSalaries();

            }
            catch (System.Exception e)
            {
                _logger.LogInformation(e.Message);
            }
            return response;
        }

        [Route("{EmployeeCode}/{filter:int}")]
        public List<SalaryDTO> GetSalarysFilter(string EmployeeCode, int filter)
        {
            List<SalaryDTO> response = new List<SalaryDTO>();
            try
            {
                response = _iSalaryService.GetSalarysFilter(EmployeeCode, filter);

            }
            catch (System.Exception e)
            {
                _logger.LogInformation(e.Message);
            }
            return response;
        }

        [HttpGet("GetOffices")]
        public List<OfficeDTO> GetOffices()
        {
            List<OfficeDTO> response = new List<OfficeDTO>();
            try
            {
                response = _iSalaryService.GetOffices();

            }
            catch (System.Exception e)
            {
                _logger.LogInformation(e.Message);
            }
            return response;
        }

        [HttpGet("GetPositios")]
        public List<PositionDTO> GetPositios()
        {
            List<PositionDTO> response = new List<PositionDTO>();
            try
            {
                response = _iSalaryService.GetPositios();

            }
            catch (System.Exception e)
            {
                _logger.LogInformation(e.Message);
            }
            return response;
        }

        [HttpGet("GetDivisios")]
        public List<DivisionDTO> GetDivisios()
        {
            List<DivisionDTO> response = new List<DivisionDTO>();
            try
            {
                response = _iSalaryService.GetDivisios();

            }
            catch (System.Exception e)
            {
                _logger.LogInformation(e.Message);
            }
            return response;
        }

        [Route("{EmployeeCode}")]
        public EmployeeDTO GetEmployee(string EmployeeCode)
        {
            EmployeeDTO response = new EmployeeDTO();
            try
            {
                response = _iSalaryService.GetEmployee(EmployeeCode);

            }
            catch (System.Exception e)
            {
                _logger.LogInformation(e.Message);
            }
            return response;
        }

        [Route("detail/{EmployeeCode}")]
        public List<SalaryDetailDTO> GetSalaryDetail(string EmployeeCode)
        {
            List<SalaryDetailDTO> response = new List<SalaryDetailDTO>();
            try
            {
                response = _iSalaryService.GetSalaryDetail(EmployeeCode);

            }
            catch (System.Exception e)
            {
                _logger.LogInformation(e.Message);
            }
            return response;
        }

        [HttpPost("save")]
        public string saveSalarys([FromBody] List<Salary> ListSalary)
        {
            var response = "";
            try
            {
                response = _iSalaryService.saveSalarys(ListSalary);

            }
            catch (System.Exception e)
            {
                _logger.LogInformation(e.Message);
            }
            return response;
        }

        [Route("bono/{EmployeeCode}")]
        public EmployeeBonoDTO GetEmployeeBono(string EmployeeCode)
        {
            EmployeeBonoDTO response = new EmployeeBonoDTO();
            try
            {
                response = _iSalaryService.GetEmployeeBono(EmployeeCode);

            }
            catch (System.Exception e)
            {
                _logger.LogInformation(e.Message);
            }
            return response;
        }

        [Route("bono/salary/{EmployeeCode}")]
        public List<SalaryBonoDTO> GetSalaryBono(string EmployeeCode)
        {
            List<SalaryBonoDTO> response = new List<SalaryBonoDTO>();
            try
            {
                response = _iSalaryService.GetSalaryBono(EmployeeCode);

            }
            catch (System.Exception e)
            {
                _logger.LogInformation(e.Message);
            }
            return response;
        }
    }
}
